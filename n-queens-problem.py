'''
input - 



output - returns a list of rows of the queen in each column if solution exists.
          if no solution, return None

[
  1, # First column queen in second row
  3, # Second column queen in fourth row
  0, # Third column queen in first row
  2, # Fourth column queen in third row
]
'''
# model chessboard
# n x n board
# n lists, n length, containing 0's

# def board_creation(n):
#   board = []
#   row = []
#   for i in range(n):
#     row.append(0)
#   for j in range(n):
#     board.append(row)
#   return board
  

# determine if_safe / looping

# four rooks -> checks row and column for safety
def is_safe(board, pos_row, pos_column):
    row = board[pos_row]

    # check safety of row
    for value in row:
        if value != 0:
            return False

    # check safety of column
    for row2 in board:
        if row2[pos_column] != 0:
            return False
            
# check diagonal safety


    return True
  
def find_positions(n):
  # build boud based on n
  board = []
  row = []
  for i in range(n):
    row.append(0)
  for j in range(n):
    board.append(row)
  return board
  
  # board contains nxn matrix

  # is safe check lives here


def n_queen(n):
  # produce board with solution set
  solution = find_positions(n)

  return solution # solved board







# find positions(n) 