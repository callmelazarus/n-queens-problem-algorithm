from pprint import pprint

def is_safe(board, pos_row, pos_column):
    row = board[pos_row]

    # check safety of row
    for value in row:
        if value != 0:
            return False

    # check safety of column
    for row2 in board:
        if row2[pos_column] != 0:
            return False
            
    return True

def place_rook_in_column(board, column_idx):
    if column_idx >= 4:
        return True

    for row_idx in range(0,4):
        if is_safe(board, row_idx, column_idx):
            board[row_idx][column_idx] = 1
            if place_rook_in_column(board, column_idx+1):
                return True
            else:
                board[row_idx][column_idx] = 0

    return False  # NO solution possible


def four_rooks():
    board = [
        [0, 0, 0, 0],
        [0, 0, 0, 0],
        [0, 0, 0, 0],
        [0, 0, 0, 0],
    ]

    place_rook_in_column(board, 0)
    return board # solved board


if __name__ == "__main__":
    board = four_rooks()
    pprint(board, width=20)
